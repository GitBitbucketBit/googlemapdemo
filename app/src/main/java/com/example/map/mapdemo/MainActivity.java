package com.example.map.mapdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng discoveryPlace = new LatLng(-26.1122834, 28.0508604);
                LatLng newsCafe = new LatLng(-26.1017591, 28.0545535);
                Marker discoveryMarker = googleMap.addMarker(new MarkerOptions().position(discoveryPlace)
                        .title("1DP Discovery Place"));

                Marker newsCafeMarker = googleMap.addMarker(new MarkerOptions().position(newsCafe)
                        .title("newsCafe"));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(discoveryPlace).zoom(13).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));

                //seems like only one description shows at a time - so the last one (news cafe) will
                //show by default and they will alternate depending on which one you click
                discoveryMarker.showInfoWindow();
                newsCafeMarker.showInfoWindow();

            }
        });
    }
}
